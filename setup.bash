#!/bin/bash -x

# run with souce setup.bash

virtualenv --python=python3 venv
activate () {
    source venv/bin/activate
}
activate

pip install -r requirements.txt

jupyter-lab
