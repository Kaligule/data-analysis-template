# Quick setup for some data analysis

This project want's to make it easy for you to get some data analysis going.

# How to?

1. Clone this repo
2. Run `setup.bash` This will:
   - setup a virtual environment
   - activate the environment
   - install some basic python packages for data analysis
   - start a jupyter lab server and open it in the browser
3. (Optional) Put your data in the `data` directory.
4. Open `example_analysis.ipynb`. There is already some (functional) example code there that works with the example dataset `data/Pokemon.csv`. Adapt it to your data, feel free to delete all the stuff you don't need.

The example data is from [here](https://www.kaggle.com/abcsds/pokemon/data) and licensed under [CC0](https://creativecommons.org/publicdomain/zero/1.0/). Both the csv- and the excel-file contain the same data.